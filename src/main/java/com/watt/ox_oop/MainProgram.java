package com.watt.ox_oop;

public class MainProgram {

    public static void main(String[] args) {
        Game game = new Game();
        game.run();
    }
}
