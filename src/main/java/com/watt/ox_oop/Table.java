package com.watt.ox_oop;

public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'},
    {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastCol;
    private int lastRow;
    private int round;

    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastRow = row;
            this.lastCol = col;
            /*round++;*/
            return true;
        }
        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void switchPlayer() {
        if (currentPlayer == playerX) {
            currentPlayer = playerO;
        } else {
            currentPlayer = playerX;
        }
    }

    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastCol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if (currentPlayer == playerO) {
            playerO.win();
            playerX.lose();
        } else {
            playerO.lose();
            playerX.win();
        }
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastRow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkObilque() {
        if (table[0][0] == 'O' && table[1][1] == 'O' && table[2][2] == 'O') {
            return;
        } else if (table[0][0] != 'X' && table[1][1] != 'X' && table[2][2]
                != 'X') {
            return;
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    public boolean checkDraw() {
        if (round == 9) {
            playerX.draw();
            playerO.draw();
            return true;
        }
        return false;
    }

    public void checkWin() {
        checkRow();
        checkCol();
        /*checkObilque();*/
        /*checkDraw();*/
    }

    public boolean isFinish() {
        return finish;
    }

    public Player getWinner() {
        return winner;
    }
}
