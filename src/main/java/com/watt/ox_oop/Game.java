package com.watt.ox_oop;

import java.util.Scanner;

public class Game {

    private Player playerX;
    private Player playerO;
    private Player turn;
    private Table table;
    private int row, col;

    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.println("Please input Row Col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;

            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Error: table at row and col is not available.");
        }
    }

    public void newGame() {
        table = new Table(playerX, playerO);
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            this.showTurn();
            this.input();
            table.checkWin();
            if (checkGame()) {
                break;
            }
            table.switchPlayer();
        }
    }

    private boolean checkGame() {
        if (table.isFinish()) {
            if (table.getWinner() == null) {
                System.out.println("Draw!!");
            } else {
                System.out.println(table.getWinner().getName() + " Win!!");
            }
            this.showTable();
            System.out.println("Do you want a play again ? Y/N");
            char a = kb.next().charAt(0);
            if (a == 'y' || a == 'Y') {
                this.newGame();
            } else {
                return true;
            }
        }
        return false;
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }
}
