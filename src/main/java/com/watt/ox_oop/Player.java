package com.watt.ox_oop;

public class Player {

    char name;
    int win;
    int lose;
    int draw;

    public Player(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public void setName(char name) {
        this.name = name;
    }

    public int getWin() {
        return win;
    }

    public void win() {
        win++;
    }

    public void lose() {
        lose++;
    }

    public void draw() {
        draw++;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

}
